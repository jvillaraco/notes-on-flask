# README.md in sqlAlchemy project

Project to test operations and funcionality of Alchemy and flask_migrate

Following:
https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-iv-database

# TOC
1. [mysql operations](#myop)
1. [virtual env](#dmde)
1. [pip packages](#vien)
1. [Environment variables](#enva)
1. [Files created](#ficr)
1. [Flask app environment execution](#faee)
1. [pip packages](#vien)
1. [Flask db operations](#fdop)
1. [Tests to do](#ttdo)
1. [Testing SQLAlchemy data type size in mariadb](#tsdtim)


# mysql operations <a name="myop"></a>
create database sqlalchemy;

CREATE USER 'sqlalchemy_user'@'localhost' IDENTIFIED BY '****';

GRANT ALL ON sqlalchemy.* TO 'sqlalchemy_user'@'localhost';


# virtual env <a name="vien"></a>
python3 -m venv venv

source venv/bin/activate

# pip packages <a name="pipa"></a>
pip install flask Flask-SQLAlchemy Flask-Migrate pymysql python-dotenv

    python
    >>> import sqlalchemy
    >>> sqlalchemy.__version__
    '1.3.16'


# Environment variables <a name="enva"></a>
* FLASK_APP=

# Files created <a name="ficr"></a>
* config.py
* app/__init__.py
* app/models.py
* app/routes.py (this file is void)

# Flask app environment execution <a name="faee"></a>
`python`

    from sqlAlchemy import app
    app.config['SQLALCHEMY_DATABASE_URI']
        'mysql+pymysql://:sqlalchemy_user:****@127.0.0.1/sqlalchemy'
or

flask shell

    app.config['SQLALCHEMY_DATABASE_URI']
        'mysql+pymysql://:sqlalchemy_user:****@127.0.0.1/sqlalchemy'



# Flask db operations <a name="fdop"></a>
flask db init

    Creating directory /home/villaraco/treball/dev/sqlAlchemy/migrations ...  done
    Creating directory /home/villaraco/treball/dev/sqlAlchemy/migrations/versions ...  done
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/env.py ...  done
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/alembic.ini ...  done
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/README ...  done
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/script.py.mako ...  done
    Please edit configuration/connection/logging settings in '/home/villaraco/treball/dev/sqlAlchemy/migrations/alembic.ini' before proceeding.

flask db migrate -m "users table"

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected added table 'user'
    INFO  [alembic.autogenerate.compare] Detected added index 'ix_user_email' on '['email']'
    INFO  [alembic.autogenerate.compare] Detected added index 'ix_user_username' on '['username']'
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/versions/338cb1bbc414_users_table.py ...  done

flask db upgrade:

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade  -> 338cb1bbc414, users table

Adding new table post in models.py and calling again to migrate:

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected added table 'post'
    INFO  [alembic.autogenerate.compare] Detected added index 'ix_post_timestamp' on '['timestamp']'
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/versions/60af5d38460f_posts_table.py ...  done

flask db upgrade:

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade 338cb1bbc414 -> 60af5d38460f, posts table

Testing:

    python
    from app import db
    from app.models import User, Post
    u = User(username='john', email='john@example.com')
    db.session.add(u)
    db.session.commit()
    users = User.query.all()
    users
        ANS: [<User john>]
    for u in users:
        print(u.id, u.username)
            ANS: 1 john

Deleting users:

    users = User.query.all()
    for u in users:
        db.session.delete(u)
    db.session.commit()
    users = User.query.all()
    users
        ANS: []



# Tests to do <a name="ttdo"></a>
1. [Modify just a table field size.](#mjtfs)
1. [What if the directory migration doesn't exists?.](#dmde)
1. [Downgrade and upgrade again](#daua)
1. [Upgrade in PRO systems] (#uips)

## Modify just a table field size. <a name="mjtfs"></a>
After tested this I realize that alembic doesn't detect this little changes in this field.

In app/models.py table Class Post From:

    body = db.Column(db.String(140))
to:

    body = db.Column(db.String(141))

`flask db migrate -m "changed Post body dnb.column to 142 chars"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.env] No changes in schema detected.

This is the explanation:
https://alembic.sqlalchemy.org/en/latest/autogenerate.html#what-does-autogenerate-detect-and-what-does-it-not-detect

And:
https://alembic.sqlalchemy.org/en/latest/autogenerate.html#compare-types

After backup lines, added compare_type=True in lines 54 and 95 at migration/
env.py it has begin to detect the litle change.

MariaDB [sqlalchemy]> `desc post;`

    +-----------+--------------+------+-----+---------+----------------+
    | Field     | Type         | Null | Key | Default | Extra          |
    +-----------+--------------+------+-----+---------+----------------+
    | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
    | body      | varchar(140) | YES  |     | NULL    |                |
    | timestamp | datetime     | YES  | MUL | NULL    |                |
    | user_id   | int(11)      | YES  | MUL | NULL    |                |
    +-----------+--------------+------+-----+---------+----------------+

`flask db migrate -m "changed Post body dnb.column to 142 chars"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected type change from VARCHAR(length=140) to String(length=143) on 'post.body'
    Generating /home/villaraco/treball/dev/sqlAlchemy/migrations/versions/2ba8573cd9f5_changed_post_body_dnb_column_to_142_.py ...  done

`flask db upgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade 60af5d38460f -> 2ba8573cd9f5, changed Post body dnb.column to 142 chars

MariaDB [sqlalchemy]> `desc post;`

    +-----------+--------------+------+-----+---------+----------------+
    | Field     | Type         | Null | Key | Default | Extra          |
    +-----------+--------------+------+-----+---------+----------------+
    | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
    | body      | varchar(143) | YES  |     | NULL    |                |
    | timestamp | datetime     | YES  | MUL | NULL    |                |
    | user_id   | int(11)      | YES  | MUL | NULL    |                |
    +-----------+--------------+------+-----+---------+----------------+


## What if the directory migration doesn't exists? <a name="dmde"></a>

`mv migrations migrations.bk20200408`

`flask db init`

    Creating directory /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations ...  done
    Creating directory /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/env.py ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/alembic.ini ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/script.py.mako ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/README ...  done
    Please edit configuration/connection/logging settings in '/home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/alembic.ini' before proceeding.

`flask db migrate -m "first commit after deleted migrations dir"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    ERROR [root] Error: Can't locate revision identified by '2ba8573cd9f5'

Following: https://stackoverflow.com/questions/32311366/alembic-util-command-error-cant-find-identifier


MariaDB [sqlalchemy]> `drop table alembic_version;`

`rm -R migrations`

(remember to modify migrations/env.py as seen before)

`flask db init`

    Creating directory /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations ...  done
    Creating directory /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/env.py ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/alembic.ini ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/script.py.mako ...  done
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/README ...  done
    Please edit configuration/connection/logging settings in '/home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/alembic.ini' before proceeding.

Modified VARCHAR(length=143) to String(length=144) on 'post.body'

`flask db migrate -m "first commit after deleted migrations dir with a mod"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected type change from VARCHAR(length=143) to String(length=144) on 'post.body'
    Generating /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions/4b59385f4ca3_first_commit_after_deleted_migrations_.py ...  done

`flask db upgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade  -> 4b59385f4ca3, first commit after deleted migrations dir with a mod

## Downgrade and upgrade again <a name="daua"></a>
`flask db history`

    <base> -> 4b59385f4ca3 (head), first commit after deleted migrations dir with a mod


`flask db history --verbose`

    Rev: 4b59385f4ca3 (head)
    Parent: <base>
    Path: /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions/4b59385f4ca3_first_commit_after_deleted_migrations_.py

        first commit after deleted migrations dir with a mod
        
        Revision ID: 4b59385f4ca3
        Revises: 
        Create Date: 2020-04-08 17:30:45.479851


MariaDB [sqlalchemy]> `desc post;`

    ...
    | body      | varchar(144) | YES  |     | NULL    |                |
    ...

`flask db downgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running downgrade 4b59385f4ca3 -> , first commit after deleted migrations dir with a mod

MariaDB [sqlalchemy]> `desc post;`

    ...
    | body      | varchar(143) | YES  |     | NULL    |                |
    ...

`flask db upgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade  -> 4b59385f4ca3, first commit after deleted migrations dir with a mod


MariaDB [sqlalchemy]> desc post;

    ...
    | body      | varchar(144) | YES  |     | NULL    |                |
    ...




## Upgrade in PRO systems <a name="uips"></a>

### Preparation for a simulated PRO environment <a name="pfspe"></a>
`rsync -av
    --exclude="__pycache__"
    --exclude="venv"
    --exclude="migrations"
    --exclude="config.py"
    /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/
    /home/villaraco/borrar/sqlalchemyPro/`


MariaDB []> `create database sqlalchemyPro;`

MariaDB []> `GRANT ALL ON sqlalchemyPro.* TO 'sqlalchemy_user'@'localhost';`

Copy config.py with modification for new database (it has been excluded in order to don't get errors):

```python
import os
from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    # ...
    TEST_VAR = 'aaa'
    # SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
    #     'sqlite:///' + os.path.join(basedir, 'app.db')
    SQLALCHEMY_DATABASE_URI = os.environ.get('DATABASE_URL') or \
        'mysql+pymysql://sqlalchemy_user:****@127.0.0.1/sqlalchemyPro'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
```

Create sqlAlchemy file and DB structure and app db structure:

`flask db init`

`flask db migrate -m "First update on PRO"`

`flask upgrade`


### Make an update on PRE system

Update post on app/models.py:
    
    body = db.Column(db.String(140))

MariaDB [sqlalchemy]> `desc post;`

    ...
    | body      | varchar(144) | YES  |     | NULL    |                |
    ...

`flask db migrate -m "Shorten Post body column size to 140 chars"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected type change from VARCHAR(length=144) to String(length=140) on 'post.body'

`flask db upgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade 4b59385f4ca3 -> 9dbf589145c4, Shorten Post body column size to 140 chars

MariaDB [sqlalchemy]> `desc post;`

    ...
    | body      | varchar(140) | YES  |     | NULL    |                |
    ...


`flask db history --verbose`

    Rev: 9dbf589145c4 (head)
    Parent: 4b59385f4ca3
    Path: /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions/9dbf589145c4_shorten_post_body_column_size_to_140_.py

        Shorten Post body column size to 140 chars
        
        Revision ID: 9dbf589145c4
        Revises: 4b59385f4ca3
        Create Date: 2020-04-08 19:06:04.358867

    Rev: 4b59385f4ca3
    Parent: <base>
    Path: /home/villaraco/treball/dev/python/notes-on-flask/sqlAlchemy/migrations/versions/4b59385f4ca3_first_commit_after_deleted_migrations_.py

        first commit after deleted migrations dir with a mod
        
        Revision ID: 4b59385f4ca3
        Revises: 
        Create Date: 2020-04-08 17:30:45.479851


### Update app on PRO env and uddate db with flask-sqlAlchemy

Use same [rsync command that previous code copy](#uips)

`flask db history --verbose`

    Rev: 579b27cbe0ac (head)
    Parent: <base>
    Path: /home/villaraco/borrar/sqlalchemyPro/migrations/versions/579b27cbe0ac_first_update_on_pro.py

        First update on PRO
        
        Revision ID: 579b27cbe0ac
        Revises: 
        Create Date: 2020-04-08 18:54:12.449641


Let's create some data in DB:

villaraco@pc06:~$ `mysql -u sqlalchemy_user -p sqlalchemyPro`

MariaDB [sqlalchemyPro]> `desc post;`

    +-----------+--------------+------+-----+---------+----------------+
    | Field     | Type         | Null | Key | Default | Extra          |
    +-----------+--------------+------+-----+---------+----------------+
    | id        | int(11)      | NO   | PRI | NULL    | auto_increment |
    | body      | varchar(144) | YES  |     | NULL    |                |
    | timestamp | datetime     | YES  | MUL | NULL    |                |
    | user_id   | int(11)      | YES  | MUL | NULL    |                |
    +-----------+--------------+------+-----+---------+----------------+

MariaDB [sqlalchemyPro]> `insert into user values (1,"u1","email1","hash1");`

MariaDB [sqlalchemyPro]> `insert into post values (1,"body1",now(),1);`

`flask db migrate -m "Update to Pro 20200408"`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.autogenerate.compare] Detected type change from VARCHAR(length=144) to String(length=140) on 'post.body'
    Generating /home/villaraco/borrar/sqlalchemyPro/migrations/versions/3882dbc18b41_update_to_pro_20200408.py ...  done

`flask db upgrade`

    INFO  [alembic.runtime.migration] Context impl MySQLImpl.
    INFO  [alembic.runtime.migration] Will assume non-transactional DDL.
    INFO  [alembic.runtime.migration] Running upgrade 579b27cbe0ac -> 3882dbc18b41, Update to Pro 20200408

MariaDB [sqlalchemyPro]> `desc post;`

    ...
    | body      | varchar(140) | YES  |     | NULL    |                |
    ...

MariaDB [sqlalchemyPro]> `select * from post;`

    +----+-------+---------------------+---------+
    | id | body  | timestamp           | user_id |
    +----+-------+---------------------+---------+
    |  1 | body1 | 2020-04-08 19:08:19 |       1 |
    +----+-------+---------------------+---------+

`flask db history --verbose`

    Rev: 3882dbc18b41 (head)
    Parent: 579b27cbe0ac
    Path: /home/villaraco/borrar/sqlalchemyPro/migrations/versions/3882dbc18b41_update_to_pro_20200408.py

        Update to Pro 20200408
        
        Revision ID: 3882dbc18b41
        Revises: 579b27cbe0ac
        Create Date: 2020-04-08 19:21:32.083746

    Rev: 579b27cbe0ac
    Parent: <base>
    Path: /home/villaraco/borrar/sqlalchemyPro/migrations/versions/579b27cbe0ac_first_update_on_pro.py

        First update on PRO
        
        Revision ID: 579b27cbe0ac
        Revises: 
        Create Date: 2020-04-08 18:54:12.449641

# Testing SQLAlchemy data type size in mariadb] <a name="tsdtim"></a>
Class Tests has been created:

    class Tests(db.Model):
        id = db.Column(db.Integer, primary_key=True)
        i_int = db.Column(db.Integer)
        i_bigint = db.Column(db.BigInteger)
        b_bool = db.Column(db.Boolean)
        t_text = db.Column(db.Text)

And its final representation in MariaDB:

    +----------+------------+------+-----+---------+----------------+
    | Field    | Type       | Null | Key | Default | Extra          |
    +----------+------------+------+-----+---------+----------------+
    | id       | int(11)    | NO   | PRI | NULL    | auto_increment |
    | i_int    | int(11)    | YES  |     | NULL    |                |
    | i_bigint | bigint(20) | YES  |     | NULL    |                |
    | b_bool   | tinyint(1) | YES  |     | NULL    |                |
    | t_text   | text       | YES  |     | NULL    |                |
    +----------+------------+------+-----+---------+----------------+

