"""Shorten Post body column size to 140 chars

Revision ID: 9dbf589145c4
Revises: 4b59385f4ca3
Create Date: 2020-04-08 19:06:04.358867

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import mysql

# revision identifiers, used by Alembic.
revision = '9dbf589145c4'
down_revision = '4b59385f4ca3'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'body',
               existing_type=mysql.VARCHAR(length=144),
               type_=sa.String(length=140),
               existing_nullable=True)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column('post', 'body',
               existing_type=sa.String(length=140),
               type_=mysql.VARCHAR(length=144),
               existing_nullable=True)
    # ### end Alembic commands ###
