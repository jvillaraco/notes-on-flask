from datetime import datetime
from hashlib import md5
from time import time
from flask import current_app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from app import db, login


class User(UserMixin, db.Model):
    # After adding repo_type we have to fill it with 0 (done 19.11.2019)
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    fail_tries = db.Column(db.Integer, default=0)
    about_me = db.Column(db.String(140))
    blocked_till = db.Column(db.DateTime)
    repo_type = db.Column(db.Integer, default=0)  # 0: local, 1: LDAP (by now)
    last_seen = db.Column(db.DateTime, default=datetime.utcnow)

    def __repr__(self):
        # return '<User {}>'.format(self.username)
        return "<User %s: id=%s email=%s pw=%s " \
               "fail_tries=%s blocked_till=%s" % (self.username,
                                                  self.id, self.email,
                                                  self.password_hash,
                                                  self.fail_tries,
                                                  self.blocked_till)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def avatar(self, size):
        digest = md5(self.email.lower().encode('utf-8')).hexdigest()
        return 'https://www.gravatar.com/avatar/{}?d=identicon&s={}'.format(
            digest, size)

    def get_reset_password_token(self, expires_in=600):
        return jwt.encode(
            {'reset_password': self.id, 'exp': time() + expires_in},
            current_app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_reset_password_token(token):
        try:
            id = jwt.decode(token, current_app.config['SECRET_KEY'],
                            algorithms=['HS256'])['reset_password']
        except:
            return
        return User.query.get(id)


@login.user_loader
def load_user(id):
    return User.query.get(int(id))


class Cert_enable(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    nom = db.Column(db.String(55), index=True, unique=True)
    mac = db.Column(db.String(17), default="00:00:00:00:00:00")
    enabled = db.Column(db.Boolean, default=False)
    enabled_by = db.Column(db.String(64), index=True)
    creaded = db.Column(db.DateTime,
                        default=datetime.utcnow,
                        index=True)
    last_mod = db.Column(db.DateTime,
                         default=datetime.utcnow,
                         index=True)
    # Has it been downloaded? (web downloads doen't count!)
    downloaded = db.Column(db.Boolean, default=False)
    # When has it been downloaded? (web downloads doen't count!)
    downloaded_at = db.Column(db.DateTime)

    def __repr__(self):
        # return '<Cert_enabled {}>'.format(self.nom)
        return "<Cert_enabled: id=%s nom=%s mac=%s " \
               "enabled=%s enabled_by=%s creaded=%s " \
               " last_mod=%s downloaded=%s downloaded_at=%s" \
                % (self.id, self.nom, self.mac, self.enabled, self.enabled_by,
                   self.creaded,  self.last_mod, self.downloaded,
                   self.downloaded_at)


class Access_log(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    access = db.Column(db.DateTime,
                       default=datetime.utcnow,
                       index=True)
    ip = db.Column(db.String(16))
    declared_user = db.Column(db.String(64))
    success_login = db.Column(db.Boolean, default=False)

    def __repr__(self):
        return "<AL: id=%s access=%s ip=%s declared_user=%s " \
               "success_login=%s>" \
               % (self.id, self.access, self.ip, self.declared_user,
                  self.success_login)


class Monitoring(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.utcnow, index=True)
    server = db.Column(db.String(20))
    concept = db.Column(db.String(64), index=True)
    value = db.Column(db.Float())

    def __repr__(self):
        return "<AL: id=%s time=%s concept=%s value=%s " %\
               (self.id, self.time, self.concept, self.value,
                self.success_login)


class Issues(db.Model):
    id = db.Column(db.BigInteger, primary_key=True)
    time = db.Column(db.DateTime, default=datetime.utcnow, index=True)
    closetime = db.Column(db.DateTime, index=True)
    reason = db.Column(db.DateTime, index=True)
    server = 