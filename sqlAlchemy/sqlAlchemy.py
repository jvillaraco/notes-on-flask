from app import app, db
from app.models import User, Post

# This decorator loads this vars in flask shell context automagically
@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'User': User, 'Post': Post}